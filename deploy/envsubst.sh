#!/bin/bash

for f in deploy/*.kube.yaml
do
    envsubst < "$f" >> deploy/manifest.yaml
    echo '---' >> deploy/manifest.yaml
done