package tr.edu.iyte.ceng555.fishservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tr.edu.iyte.ceng555.fishservice.service.FishService;

@RestController
@RequestMapping("/fish")
public class FishController {

    private final FishService fishService;

    @Autowired
    public FishController(FishService fishService) {
        this.fishService = fishService;
    }

}
