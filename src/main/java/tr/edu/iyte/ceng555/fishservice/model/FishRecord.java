package tr.edu.iyte.ceng555.fishservice.model;

public class FishRecord {

    private String type;
    private int price;
    private boolean sold;
    private long fishermanId;
    private long buyerId;
    private int amount;
    private String unit;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isSold() {
        return sold;
    }

    public void setSold(boolean sold) {
        this.sold = sold;
    }

    public long getFishermanId() {
        return fishermanId;
    }

    public void setFishermanId(long fishermanId) {
        this.fishermanId = fishermanId;
    }

    public long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(long buyerId) {
        this.buyerId = buyerId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
