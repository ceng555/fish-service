package tr.edu.iyte.ceng555.fishservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import tr.edu.iyte.ceng555.fishservice.model.FishRecord;

@Service
public class FishService {

    private final Logger logger = LoggerFactory.getLogger(FishService.class);
    private final ObjectMapper objectMapper = new ObjectMapper();

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public FishService(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @KafkaListener(topics = "auction-finished", groupId = "groupId")
    public void consumeAuctionFinished(String message) throws JsonProcessingException {
        logger.info("Topic: auction-finished, Message: {}", message);
        FishRecord fishRecord = objectMapper.readValue(message, FishRecord.class);
        checkTheRecord(fishRecord);
    }

    public void checkTheRecord(FishRecord record) throws JsonProcessingException {
        if (record.isSold()) {
            kafkaTemplate.send("record-finished", objectMapper.writeValueAsString(record));
        } else {
            kafkaTemplate.send("record-not-finished", objectMapper.writeValueAsString(record));
        }
    }

}
